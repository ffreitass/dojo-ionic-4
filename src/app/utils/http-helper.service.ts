import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpHelperService {
    token = 'd75a0b242e125e77e6c26aeadbee7b5f';

    constructor(private http: HttpClient) { }

    doHttpPost(url, data) {
        console.log(url);
        return new Promise((resolve, reject) => {
            this.http.post(url, JSON.stringify(data))
                .subscribe((response) => {
                    console.log(response);
                    resolve(response);
                }, (err) => {
                    console.log(err);
                    reject(err);
                });
        });
    }

    doHttpGet(url) {
        console.log(url);
        return new Promise((resolve, reject) => {
            this.http.get(url).subscribe((response) => {
                console.log(response);
                resolve(response);
            }, (err) => {
                console.log(err);
                reject(err);
            });
        });
    }
}
