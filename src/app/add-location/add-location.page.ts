import { Component, OnInit } from '@angular/core';
import { HttpHelperService } from '../utils/http-helper.service';

@Component({
  selector: 'app-add-location',
  templateUrl: './add-location.page.html',
  styleUrls: ['./add-location.page.scss'],
})
export class AddLocationPage implements OnInit {
    search: string;
    type: string;
    list: any[] = [];

  constructor(private httpHelper: HttpHelperService) { }

  ngOnInit() {

  }




  searchCity(serach, type) {
    const token = 'd75a0b242e125e77e6c26aeadbee7b5f';
    const url = 'http://apiadvisor.climatempo.com.br/api/v1/locale/city?' + type + '=' + serach + '&token=' + token;

    console.log(url);
    this.httpHelper.doHttpGet(url)
        .then((response: any[]) => {
            this.list = response;
        })
        .catch((error) => {
            console.log(JSON.stringify(error));
        });
  }

}
