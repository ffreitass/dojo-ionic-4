export class PrevisaoTempo {
    temperature: string;
    wind_direction: string;
    wind_velocity: string;
    humidity: string;
    condition: string;
    pressure: string;
    icon: string;
    sensation: string;
    date: string;

    constructor() {}
}
