import { PrevisaoTempo } from './previsao-tempo';

export class Cidade {

    id: number;
    name: string;
    state: string;
    country: string;
    data: PrevisaoTempo[] = new Array();

    constructor() { }
}
