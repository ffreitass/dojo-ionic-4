dojo-ionic-4

*************************************************************************************************
 Este DoJo aqui descrito está incompleto e será populado com maiores detalhes conforme o tempo
*************************************************************************************************

A  ideia central deste DoJo é a construção de uma App simples para um primeiro nivelamento de equipe.

O projeto a ser desenvolvido é um app de previsão do tempo utilizando a API do climatempo.

Para a utilização desta API é necessário um token que pode ser adquirido diretamente no site abaixo.

https://advisor.climatempo.com.br/home

Este app terá 3 telas:
-Home - tela inicial com uma lista de cidades preferidas e algumas informações básicas sobre as condições climáticas atuais.
-Add-City - tela para pesquisa e adicição de cidades à lista de preferidas. A pesquisa pode ser feita por cidade ou por uf
-Details-City - tela com o detalhamento completo das condições climáticas da cidade.

Este DoJo considera a já existencia das ferramentas e frameworks instaladas e funcionais na máquina.

Roadmap

1. criar o projeto utilizando o comando abaixo pelo terminal
ionic start <nome_projeto> <tipo_projeto>
- <nome_projeto>
- <tipo_projeto>
    - blank
    - sidemenu
    - tabs

Este comando irá criar uma pasta com o nome do projeto e uma estrutura básica do projeto inicial escolhido.

2. acessar a pasta do projeto pelo terminal

3. abrir o projeto em seu editor de preferencia

4. pelo terminal, criar duas páginas com os comando abaixo

- ionic generate page AddCity

- ionic generate page DetailsCity

5. criar na Home uma forma de navegar para a página de adição de cidade
Vamos criar uma estutura básica de navegação

6. na página de adição de cidade, criar a pesquisa e fazer a comunicação e manipulação dos resultados retornados pela API, bem como a adição da cidade escolhida a lista de favoritas e a navegação para a tela de detalhes para a verificação das condições climáticas do local.

- exemplos de chamada:

    - http://apiadvisor.climatempo.com.br/api/v1/locale/city?name=<CIDADE>&token=<TOKEN>

    - http://apiadvisor.climatempo.com.br/api/v1/locale/city?state=<UF>&token=<TOKEN>

    - http://apiadvisor.climatempo.com.br/api/v1/locale/city?name=<CIDADE>&state=<UF>&token=<TOKEN>

- exemplo de retorno

    - [{"id":5575,"name":"Canoas","state":"RS","country":"BR  "}]

7. na página de detalhes, criar a visualização para os dados de condição climática

8. na Home, criar uma lista para a visualização básica das condições climáticas das cidades favoritas